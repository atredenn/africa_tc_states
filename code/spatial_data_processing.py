from osgeo import gdal, gdalconst, osr, ogr
import matplotlib.pyplot as plt
import numpy as np
import os, errno
import warnings
import copy



####
#### Set Path
####
path = "/Users/atredenn/Repos/africa_tc_states/data/"
os.chdir(path)
execfile('../code/raster_utilities.py')
woodycover_file = "woodypercent_Multi5_Jul3_2016.tif"



####
#### Function to remove file if it exists
####
def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred



####
#### Information from ENVI header file for tree cover map
####
""" FROM ENVI HEADER FILE FOR WOODY COVER TIF
map_info = {Sinusoidal, 1.0000, 1.0000, -1996473.8200, 2444697.7300, 9.9995411100e+002, 9.9995411100e+002, WGS-84, units=Meters}
projection_info = {16, 6370997.0, 0.000000, 0.0, 0.0, WGS-84, Sinusoidal, units=Meters}
crs = {PROJCS["Sinusoidal",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Sinusoidal"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",0.0],UNIT["Meter",1.0]]}
"""



####
#### 1. Match extent and size of woody cover to precip map
####
# Read in woody cover to transform, set some params from ENVI info above
inputfile = woodycover_file
input = gdal.Open(inputfile, gdalconst.GA_Update)
input.SetGeoTransform([-1996473.8200, 999.9541110000001, 0.0, 2444697.7300, 0.0, -999.9541109999999])
inputTrans = input.GetGeoTransform()
inputProj = 'PROJCS["Sinusoidal",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Sinusoidal"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",0.0],UNIT["Meter",1.0]]'

# Read in precip as the reference map we want to match
referencefile = "MAP_1976to2005_SSA.tif"
reference = gdal.Open(referencefile, gdalconst.GA_ReadOnly)
referenceProj = reference.GetProjection()
referenceTrans = reference.GetGeoTransform()
bandreference = reference.GetRasterBand(1)    
x = reference.RasterXSize 
y = reference.RasterYSize

# Create new raster
outputfile = "woodypercent_resample.tif"
driver = gdal.GetDriverByName('GTiff')
output = driver.Create(outputfile,x,y,1,bandreference.DataType)
output.SetGeoTransform(referenceTrans)
output.SetProjection(referenceProj)

# Resample woody cover to match precip, save as new file
gdal.ReprojectImage(input,output,inputProj,referenceProj,gdalconst.GRA_Bilinear)

# Clear it out
del output
del driver



####
#### 2. Clip the new raster to SSA shapefile using command line tools
####
# Remove the clipped file if present
silentremove('ssa_woodypercent_resample.tif')

# Resample using command line tools of GDAL
os.system('gdalwarp --config GDALWARP_IGNORE_BAD_CUTLINE YES \
            -dstnodata 255 -q -cutline \
            ./africa_shape_files/ssa_poly.shp \
            -crop_to_cutline \
            woodypercent_resample.tif \
            ssa_woodypercent_resample.tif')

# Remove the intermediate resampled file
silentremove('woodypercent_resample.tif')



####
#### 3. Rematch extent of clipped woody cover to precip map (extra row is added above)
####
# Create clipper shapefile from the SSA MAP raster (our base map)
os.system('gdaltindex clipper.shp MAP_1976to2005_SSA.tif')

# Clip the resampled file to match base map extent and cell size
silentremove('ssa_woodypercent_for_analysis.tif')
os.system('gdalwarp -cutline clipper.shp \
            -crop_to_cutline \
            ssa_woodypercent_resample.tif \
            ssa_woodypercent_for_analysis.tif')

# Remove intermediate files
silentremove('clipper.shp')
silentremove('clipper.dbf')
silentremove('clipper.prj')
silentremove('clipper.shx')
silentremove('ssa_woodypercent_resample.tif')



####
#### 5. Mask out ag, urban, water and bare land (Xu et al. Ecology methods)
####
# Read in the relevant rasters
landmask, landmask_md = raster2array("./ssa_landmask.tif")
ssacover, ssacover_md = raster2array("./ssa_woodypercent_for_analysis.tif")
#ssarain, ssarain_md = raster2array("./MAP_1976to2005_SSA.tif")

# Add nan to MAP first
#ssarain[ssarain < 0] = np.nan

# Do the masking
ssacover_mask = copy.copy(ssacover)
ssacover_mask[landmask == 1] = np.nan
#ssarain_mask = copy.copy(ssarain)
#ssarain_mask[landmask == 1] = np.nan

# Set certain values to nan for tree cover
namask = ~np.isnan(ssacover_mask)
namask[namask] &= ssacover_mask[namask] > 100
ssacover_mask[namask] = np.nan
namask = ~np.isnan(ssacover_mask)
namask[namask] &= ssacover_mask[namask] < 0
ssacover_mask[namask] = np.nan
namask = np.isnan(ssacover_mask)
ssacover_mask[namask] = 254

# Quick check 
count1 = np.count_nonzero(~np.isnan(ssacover))
count2 = np.count_nonzero(~np.isnan(ssacover_mask))
if count1==count2:
    warnings.warn("Masking of landcover may not have worked.",RuntimeWarning)

# Convert array to raster and save as new tif
masked_cover = array2raster("ssa_cover_masked_analysis.tif", ssacover_mask, ssacover_md)



####
#### Extra code for initial checks, all working now...
####
# Some checks 1
"""
tc = output.ReadAsArray()
tc[tc==np.nanmax(tc)]=np.nan
np.nanmin(tc)
np.nanmax(tc)
plt.imshow(tc)

ppt = reference.ReadAsArray()
plt.imshow(ppt)
plt.colorbar()

tc.shape
ppt.shape
"""

# Some checks 2
"""
file = "ssa_woodypercent_resample.tif"
ssa_tc = gdal.Open(file, gdalconst.GA_ReadOnly)
ssa_tc.RasterXSize
ssa_tc.RasterYSize
ssa_tc.GetGeoTransform()
tc = ssa_tc.ReadAsArray()
mask = ~np.isnan(tc)
mask[mask] &= tc[mask] > 100
tc[mask] = np.nan

np.nanmin(tc)
np.nanmax(tc)
plt.imshow(tc); plt.colorbar()
"""

# Some checks 3
"""
file2 = "ssa_woodypercent_for_analysis.tif"
ssa_tc2 = gdal.Open(file2, gdalconst.GA_ReadOnly)
ssa_tc2.RasterXSize
ssa_tc2.RasterYSize
ssa_tc2.GetGeoTransform()
tc2 = ssa_tc2.ReadAsArray()
mask = ~np.isnan(tc2)
mask[mask] &= tc2[mask] > 100
tc2[mask] = np.nan
mask = ~np.isnan(tc2)
mask[mask] &= tc2[mask] < 0
tc2[mask] = np.nan

np.nanmin(tc2)
np.nanmax(tc2)
plt.imshow(tc2); plt.colorbar()

plt.imshow(ssacover_mask); plt.colorbar()
"""

